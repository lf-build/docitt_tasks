using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace Docitt.Tasks.AddCalendarEventReminder
{    
    public class Configuration : IDependencyConfiguration
    {
        public string NoOfDays { get; set; }
        public string Description { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}