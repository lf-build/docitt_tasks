﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Docitt.EventManagement.Client;
using Docitt.ReminderService.Client;
using Docitt.UserProfile.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Tasks;
using LendFoundry.Tenant.Client;
using DependencyInjection = LendFoundry.Foundation.Services.DependencyInjection;

namespace Docitt.Tasks.AddCalendarEventReminder
{
    public class Program : DependencyInjection
    {
        public static void Main()
        {
            Program p = new Program();
            p.Provider.GetService<IAgent>().Execute();
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddTenantService();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddEventManagement();
            services.AddReminderService();
            services.AddEventHub(Settings.ServiceName);
            services.AddUserProfileService();
            services.AddConfigurationService<Configuration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName);
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddTransient<IAgent, Agent>();

            return services;
        }
    }
}