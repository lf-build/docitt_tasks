
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Agent;
using System;
using Docitt.EventManagement.Client;
using System.Linq;
using System.Threading.Tasks;
using Docitt.ReminderService.Client;
using Docitt.UserProfile.Client;
using LendFoundry.Tenant.Client;

namespace Docitt.Tasks.AddCalendarEventReminder
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory,
            IEventManagementClientFactory eventManagementFactory,
            IReminderServiceClientFactory reminderServiceFactory,
            IUserProfileFactory userProfileFactory,
            ILoggerFactory loggerFactory,ITenantServiceFactory tenantServiceFactory)
            : base(tokenHandler, configurationFactory, tenantTimeFactory,tenantServiceFactory,loggerFactory, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            LoggerFactory = loggerFactory;
            EventManagementFactory = eventManagementFactory;
            ReminderServiceFactory = reminderServiceFactory;
            UserProfileFactory = userProfileFactory;
        }

        private IEventManagementClientFactory EventManagementFactory { get; }

        IReminderServiceClientFactory ReminderServiceFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private ITenantTimeFactory TenantTimeFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private ILoggerFactory LoggerFactory { get; }
        private IUserProfileFactory UserProfileFactory { get; }
        public override async void OnSchedule(string tenantId)
        {
            var token = TokenHandler.Issue(tenantId, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);

            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var eventManagement = EventManagementFactory.Create(reader);
            var reminderService = ReminderServiceFactory.Create(reader);
            var configuration = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            var userProfile = UserProfileFactory.Create(reader);
            if (configuration == null)
            {
                logger.Error($"Configuration not found related to calender event reminder Task for {tenantId}");
                return;
            }
            
            var calenderEventReminderConfiguration = configuration.Get();
            if (calenderEventReminderConfiguration == null)
            {
                logger.Error("configuration related to calender event reminder not found for {tenantId}.");
                return;
            }
            if (string.IsNullOrEmpty(calenderEventReminderConfiguration.NoOfDays))
            {
                logger.Error("configuration related to calender reminder days not found for {tenantId}.");
                return;
            }

            var flag = int.TryParse(calenderEventReminderConfiguration.NoOfDays, out var noofDays);
            if (!flag || noofDays == 0)
            {
                logger.Warn("configuration related to calender reminder days not found for {tenantId}.");
                return;
            }

            try
            {
                var eventDate = tenantTime.Today.Date.AddDays(noofDays);
                var eventlist = (await eventManagement.GetByDate("application", eventDate))?.ToList();

                if (eventlist == null || !eventlist.Any())
                {
                    logger.Info($"No event data found for {tenantId}.");
                    return;
                }

                logger.Info($"Start calendar event reminder task on :{DateTime.Now}: Time Zone : {tenantTime.TimeZone} for {tenantId}");

                Parallel.ForEach(eventlist, eventData =>
               {
                   try
                   {
                       if (!string.IsNullOrWhiteSpace(eventData.EntityId))
                       {
                           ReminderService.RequestPayload objReminderServicePayload = new ReminderService.RequestPayload();
                           var monthinfo = String.Format("{0:MMMM}", eventData.EventDate) + " " + eventData.EventDate.Date.Day;
                           objReminderServicePayload.Description = string.Format(calenderEventReminderConfiguration.Description, monthinfo);

                           var invitiesList = eventData.Invities.Split(',');
                           if (invitiesList.Length > 0)
                           {
                               foreach (var user in invitiesList)
                               {
                                   var userSettings = userProfile.GetAllSettings(user).Result;
                                   if (userSettings != null && userSettings.Count > 0)
                                   {
                                       if (userSettings.TryGetValue("calendarnotification", out var calenderNotification))
                                       {
                                           if (bool.TryParse(Convert.ToString(calenderNotification), out var result))
                                               if (result)
                                                   reminderService.Add(eventData.EntityType, eventData.EntityId, user, objReminderServicePayload);
                                       }
                                   }
                               }
                           }

                           logger.Info($"Calendar reminder for application: {eventData.EntityId} has been successfully added for {tenantId}.");
                       }
                   }
                   catch (Exception ex)
                   {
                       logger.Error($"Failed attempt to add reminder for calender event. Application: {eventData.EntityId} for {tenantId}", ex);
                   }
               });
                logger.Info($"End for {tenantId}");
            }
            catch (Exception ex)
            {
                logger.Error("Failed attempt to add reminder for calendar events for {tenantId}.", ex);
            }
        }
    }
}