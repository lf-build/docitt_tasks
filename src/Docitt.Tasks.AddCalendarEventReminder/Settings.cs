using System;

namespace Docitt.Tasks.AddCalendarEventReminder
{
    public static class Settings
    {        
         public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "task-addcalendarevent-reminder";
    }
}