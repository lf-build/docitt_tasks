﻿using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Agent;
using System;
using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Tenant.Client;

namespace Docitt.Tasks.PlaidDelink
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory,        
            ILoggerFactory loggerFactory,
            IDccPlaidServiceFactory dccPlaidServiceFactory,ITenantServiceFactory tenantServiceFactory
             ) : base(tokenHandler, configurationFactory, tenantTimeFactory,tenantServiceFactory,loggerFactory, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            Logger = LoggerFactory.Create(NullLogContext.Instance);
            DccPlaidServiceFactory = dccPlaidServiceFactory;
        }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private ILoggerFactory LoggerFactory { get; }

        private ILogger Logger { get; }

        private IConfiguration PlaidConfiguration { get; set; }

        private IDccPlaidServiceFactory DccPlaidServiceFactory { get; }

        /// <summary>
        /// OnSchedule Override 
        /// </summary>
        public override async void OnSchedule(string tenantId)
        {
            var token = TokenHandler.Issue(tenantId, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);

            var configuration = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            ReadConfiguration(configuration,tenantId);

            try
            {
                Logger.Info($"Create DccPlaidService client for {tenantId} ");
                var client = DccPlaidServiceFactory.Create(reader);
                var result = await client.DelinkItems(PlaidConfiguration.NoOfDays);
                Logger.Info($"Executed {string.Join("", result)} for {tenantId}");
            }
            catch (Exception ex)
            {
                Logger.Error($"Failed attempt to execute plaid delink for {tenantId} {ex.Message}", ex);
            }
        }

        /// <summary>
        /// Read Configuration 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="tenantId"></param>
        private void ReadConfiguration(IConfigurationService<Configuration> configuration, string tenantId)
        {
            if (configuration == null)
            {
                Logger.Error($"Error Configuration not found related to plaid-delink-task for {tenantId}");
                return;
            }

            //// Read configuration 
            PlaidConfiguration = configuration.Get();
            if (PlaidConfiguration == null)
            {
                Logger.Error($"Error Configuration related to plaid delink not found for {tenantId}.");
                return;
            }

            //// Validate NoOfDays
            if (PlaidConfiguration.NoOfDays == 0)
            {
                Logger.Error($"Configuration related to NoOfDays not found for {tenantId}.");
            }
        }
    }
}