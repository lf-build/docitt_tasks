﻿using System.Collections.Generic;

namespace Docitt.Tasks.PlaidDelink
{
    public class Configuration : IConfiguration
    {
        public int NoOfDays { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}