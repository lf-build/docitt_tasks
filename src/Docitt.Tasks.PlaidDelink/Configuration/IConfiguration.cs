﻿using LendFoundry.Foundation.Client;

namespace Docitt.Tasks.PlaidDelink
{
    public interface IConfiguration : IDependencyConfiguration
    {
        int NoOfDays { get; set; }
    }
}
