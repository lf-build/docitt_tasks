﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Docitt.Syndication.Plaid.Client;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Tasks;
using DependencyInjection = LendFoundry.Foundation.Services.DependencyInjection;

namespace Docitt.Tasks.PlaidDelink
{
    public class Program : DependencyInjection
    {
        public static void Main(string[] args)
        {            
             Program p = new Program();
             p.Provider.GetService<IAgent>().Execute();            
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddTenantService();
            services.AddDccPlaidService();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddEventHub(Settings.ServiceName);
            services.AddConfigurationService<Configuration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName);
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddTransient<IAgent, Agent>();
            services.AddTransient<IConfiguration, Configuration>();

            return services;
        }
    }
}