using System;

namespace Docitt.Tasks.PlaidDelink
{
    public static class Settings
    {
        
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "task-plaid-delink";
    }
}